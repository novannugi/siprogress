from django.shortcuts import redirect, render
from siprogress.globals import Globals
from django.db import connection
from django.db import connections
from django.http import HttpResponse
import datetime

import json
from django.core.serializers.json import DjangoJSONEncoder

# Create your views here.
def dashboard(request):
	# Check Session
	# print('masuk')
	if 'userauth' in request.session:
		is_login = request.session['userauth']
		user_priv = request.session['user_priv']
	else:
		is_login = False

	# Code
	# print(user_priv)
	if(is_login):
		# print(user_priv)
		#select name,icon,url from PRIVILEGE_MODUL, PROPERTIES_MENU where PRIVILEGE_MODUL.MENU_PRIV = PROPERTIES_MENU.id;
		qnavbar = "SELECT * FROM PRIVILEGE_NAVBAR A RIGHT JOIN PROPERTIES_NAVBAR B ON A.NAVBAR_PRIV = B.id WHERE A.USER_PRIV = '"+user_priv+"' AND B.modul = 'e-komplain' ORDER BY B.parent ASC, B.nav_order ASC;"
		qmenubar = "SELECT * FROM PRIVILEGE_MENUBAR AS A RIGHT JOIN PROPERTIES_MENUBAR AS B ON A.MENUBAR_PRIV = B.id WHERE A.USER_PRIV = '"+user_priv+"' AND B.modul = 'e-komplain' AND B.submodul is null;"
		navbars = Globals().getData(qnavbar)
		menubars = Globals().getData(qmenubar)
		print(menubars)
		#query = "select CAST(Privilege as varchar) as priv from TbPrivilege where PrivilegeId = '"+user_priv+"'"
		#cursor = connection.cursor()
		#cursor.execute(query)
		#results = dictfetchall(cursor)  
		#response = render(request, 'dashboard/home.html', {'results':results})
		response = render(request, 'dashboard/home.html', {'navbars':navbars, 'menubars':menubars})
		response['Cache-Control'] = 'no-cache, no-store, max-age=0, must-revalidate'
		return response
	else:
		return redirect('/login')

def divisi(request):
	if 'userauth' in request.session:
		is_login = request.session['userauth']
		user_priv = request.session['user_priv']
	else:
		is_login = False

		# Code
	# print(user_priv)
	if(is_login):
		# print(user_priv)
		#select name,icon,url from PRIVILEGE_MODUL, PROPERTIES_MENU where PRIVILEGE_MODUL.MENU_PRIV = PROPERTIES_MENU.id;
		qnavbar = "SELECT * FROM PRIVILEGE_NAVBAR A RIGHT JOIN PROPERTIES_NAVBAR B ON A.NAVBAR_PRIV = B.id WHERE A.USER_PRIV = '"+user_priv+"' AND B.modul = 'e-komplain' ORDER BY B.parent ASC, B.nav_order ASC;"
		qmenubar = "SELECT * FROM PRIVILEGE_MENUBAR AS A RIGHT JOIN PROPERTIES_MENUBAR AS B ON A.MENUBAR_PRIV = B.id WHERE A.USER_PRIV = '"+user_priv+"' AND B.modul = 'e-komplain' AND B.submodul = 'divisi' order by menu_order asc, id asc;"
		navbars = Globals().getData(qnavbar)
		menubars = Globals().getData(qmenubar)
		print(menubars)
		#query = "select CAST(Privilege as varchar) as priv from TbPrivilege where PrivilegeId = '"+user_priv+"'"
		#cursor = connection.cursor()
		#cursor.execute(query)
		#results = dictfetchall(cursor)  
		#response = render(request, 'dashboard/home.html', {'results':results})
		response = render(request, 'divisi/home.html', {'navbars':navbars, 'menubars':menubars})
		response['Cache-Control'] = 'no-cache, no-store, max-age=0, must-revalidate'
		return response
	else:
		return redirect('/login')

def aud_divisi(request): # aud itu add, update , delete
	cursor = connection.cursor() #buka cursor untuk koneksi ke sql server
	query = """ exec EKP_AUD_DIVISI 
		%s ,
		%s ,
		%s ,
		%s
	""" #query sp
	param = [
		Globals().input(request.POST, 'kode_divisi'),
		Globals().input(request.POST, 'nama_divisi'),
		Globals().input(request.POST, 'status_divisi'),
		Globals().input(request.POST,'status_aud','A')
	]
	cursor.execute(query, param) #PROSES EXEKUSI SP NYA
	result = Globals().fetchResult(cursor, 5) #MENGAMBIL HASIL EKSEKUSI SP 

	json_data = json.dumps(result,cls=DjangoJSONEncoder) #convert data result jadi json
	return HttpResponse(json_data, content_type="application/json")

def getAll(request):
	query = " SELECT * FROM MDDIVISI "
	result = Globals().getData(query)
	json_data = json.dumps(result,cls=DjangoJSONEncoder) #convert data result jadi json
	return HttpResponse(json_data, content_type="application/json")
