from django.urls import re_path
from django.urls import include, path

from dashboard import privelige
from . import views
from django.conf.urls import url

urlpatterns = [
    url(r'^$', views.dashboard, name='dashboard'),
    url(
        r"^privilege/",
        include(
            [
                url(r"^$", privelige.dashboard, name="privilege"),
                url(r"^menu/", privelige.menu, name="menu"),
                url(r"^menu_priv/", privelige.menu_priv, name="menu_priv"),
                url(r"^menu_post", privelige.menu_post, name="menu_post"),
                url(r"^user/", privelige.user, name="user"),
                url(r"^navbars/", privelige.navbars, name="navbars"),
                url(r"^navbars_priv/", privelige.navbars_priv, name="navbars_priv"),
                url(r"^navbars_post", privelige.navbars_post, name="navbars_post"),
                url(
                    r"^subnavbars_priv/",
                    privelige.subnavbars_priv,
                    name="subnavbars_priv",
                ),
                url(
                    r"^subnavbars_post",
                    privelige.subnavbars_post,
                    name="subnavbars_post",
                ),
                url(r"^menubars/", privelige.menubars, name="menubars"),
                url(r"^menubars_priv/", privelige.menubars_priv, name="menubars_priv"),
                url(
                    r"^submenubars_priv/",
                    privelige.submenubars_priv,
                    name="submenubars_priv",
                ),
                url(r"^menubars_post", privelige.menubars_post, name="menubars_post"),
                url(
                    r"^submenubars_post",
                    privelige.submenubars_post,
                    name="submenubars_post",
                ),
            ]
        ),
    ),

    url(r'^divisi/', 
        include(
            [
                url(r'^$',views.divisi, name='divisi'),
                url(r'^simpan',views.aud_divisi, name='aud_divisi'),
                url(r'^getAll',views.getAll, name='getAll'),
            ]
        )
    )                
]